import { Column, Entity, ObjectIdColumn, PrimaryColumn } from "typeorm";


@Entity()
export class Lesson {
    
    @ObjectIdColumn() // obj id for the mongodb 
    _id: string;

    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    startDate: string;

    @Column()
    endDate: string;
}