import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { stringify } from "querystring";
import { LessonService } from "./lesson.service";
import { LessonType } from "./lesson.type";

@Resolver(of => LessonType)
export class LessonResolver{
    constructor(
        private lessonService: LessonService
    ){}
// here we can define our Queries or Mutation
//Queries is for retrive data
//Mutation is for create new or change existing data

@Query(returns => LessonType)
lesson() {
return {
    id: 'aaa',
    name: 'Physics class',
    startDate: (new Date()).toISOString(),
    endDate: (new Date()).toISOString()
};

}

@Mutation(returns => LessonType)
createLesson(// for arguments
    @Args('name') name: string,
    @Args('startDate') startDate: string,
    @Args('endDate') endDate: string
) {
     return this.lessonService.createLesson(name, startDate, endDate);
                     
     
}
}